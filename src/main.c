#include <string.h>
#include <stdlib.h>
#include <glib-unix.h>
#include "simplecrawler.h"

typedef struct {
  GMainLoop *mainloop;
  ScCrawler *crawler;
  gboolean storecontents;
  GTree *urls;
} Data;

typedef struct {
  Data *data;
  gpointer digest;
  ScLocalStorageManager *manager;
} DataRequest;

static void finished_cb(gpointer user_data) {
  DataRequest *datarequest = user_data;
  g_main_loop_quit(datarequest->data->mainloop);
}

/*
 * gen_chksum_md5 - function used to convert
 *                  url to 16 byte md5 hash, this
 *                  hash used to compare urls to
 *                  determine if it is not already fetched.
 */
static gpointer gen_chksum_md5(const gchar *url) {
  gsize size = g_checksum_type_get_length(G_CHECKSUM_MD5);
  GChecksum *checksum = g_checksum_new(G_CHECKSUM_MD5);
  gpointer digest = g_malloc0(size);
  g_checksum_update(checksum, url, -1);
  g_checksum_get_digest(checksum, digest, &size);
  g_checksum_free(checksum);
  return digest;
}

static void next_cb(const gchar *url, gpointer user_data) {
  DataRequest *datarequest = user_data;
  DataRequest *datarequestnew = g_new0(DataRequest, 1);
  datarequestnew->data = datarequest->data;
  datarequestnew->digest = gen_chksum_md5(url);
  sc_crawler_crawl(datarequest->data->crawler,
		   url,
		   datarequestnew->digest,
		   datarequestnew);
}

static void crawled_cb(ScCrawlerRequest *request,
		       gpointer user_data) {
  DataRequest *datarequest = user_data;
  if(datarequest->manager) {
    sc_local_storage_manager_free(datarequest->manager);
  }
  g_free(datarequest);
}

static void got_chunk_cb(ScCrawlerRequest *request,
			 gpointer user_data) {
  DataRequest *datarequest = user_data;
  g_debug(G_STRLOC ": got %d length chunk for %s", request->len, request->url);
  if(datarequest->manager) {
    sc_local_storage_manager_handle_chunk(datarequest->manager,
					  request->buffer,
					  request->len);
  }
}

static void new_link_found_cb(ScCrawlerRequest *request,
			      const gchar *url,
			      gpointer user_data) {
  DataRequest *datarequest = user_data;
  SoupURI *uri = NULL;
  gpointer *digest = gen_chksum_md5(url);

  g_debug(G_STRLOC ": lookup: %s", url);
  if(g_tree_lookup(datarequest->data->urls, digest)) {
    g_free(digest);
    return;
  }
  g_tree_insert(datarequest->data->urls, digest, digest);
  uri = soup_uri_new(url);
  if(soup_uri_host_equal(request->uri, uri)) {
    DataRequest *datarequestnew = g_new0(DataRequest, 1);
    datarequestnew->data = datarequest->data;
    datarequestnew->digest = digest;
    sc_crawler_crawl(datarequest->data->crawler,
		     url,
		     digest,
		     datarequestnew);
  }
}

static gboolean got_headers_cb(ScCrawlerRequest *request,
			       gpointer user_data) {
  DataRequest *datarequest = user_data;

  g_debug(G_STRLOC ":statuscode: %d url: %s", request->statuscode, request->url);
  if(request->statuscode != 200) return FALSE;
  if(datarequest->data->storecontents && !datarequest->manager) {
    datarequest->manager = sc_local_storage_manager_new(request->url);
    return (datarequest->manager ? TRUE : FALSE);
  }
  else {
    return (request->contenttype &&
	    g_str_has_prefix(request->contenttype, "text/html")
	    ? TRUE : FALSE);
  }
}

int main(int argc, char *argv[]) {
  Data *data = NULL;
  gint iter = 3;
  glong maxlinks = 0;
  
  if(argc < 4 || g_strcmp0("--help", argv[1]) == 0 || g_strcmp0("-h", argv[1]) == 0) {
    g_print("[usage] %s maxlinks storecontents url [...]\n"
	    "\tmaxlinks - maximum number of links to process (0 means unlimited)\n"
	    "\tstorecontents - 1 to store contents (0 not to store contents)\n",
	    argv[0]);
    return 1;
  }

  maxlinks = strtol(argv[1], NULL, 10);
  maxlinks = (maxlinks == LONG_MIN || maxlinks == LONG_MAX ? 0 : maxlinks);
  data = g_new0(Data, 1);
  data->mainloop = g_main_loop_new(NULL, TRUE);
  data->crawler = sc_crawler_new(finished_cb,
				 got_headers_cb,
				 got_chunk_cb,
				 new_link_found_cb,
				 crawled_cb,
				 next_cb,
				 maxlinks);
  data->storecontents = (argv[2] && strlen(argv[2]) && argv[2][0] == '1' ? TRUE : FALSE);
  data->urls = g_tree_new_full(sc_crawler_compare_func,
			       (gpointer) g_checksum_type_get_length(G_CHECKSUM_MD5),
			       g_free,
			       NULL);
  while(iter < argc) {
    gchar *url = (g_str_has_prefix(argv[iter], "http") ?
		  g_strdup(argv[iter]) :
		  g_strdup_printf("http://%s", argv[iter]));
    DataRequest *datarequest = g_new0(DataRequest, 1);
    datarequest->data = data;
    datarequest->digest = gen_chksum_md5(url);
    g_tree_insert(data->urls, datarequest->digest, datarequest->digest);
    sc_crawler_crawl(data->crawler,
		     url,
		     datarequest->digest,
		     datarequest);
    g_free(url);
    iter++;
  }
  
  g_main_loop_run(data->mainloop);
  g_main_loop_unref(data->mainloop);
  sc_crawler_free(data->crawler);
  g_tree_unref(data->urls);
  g_free(data);
  return 0;
}

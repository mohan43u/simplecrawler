#include <string.h>
#include <glib-unix.h>
#include "simplecrawler.h"

static void sc_crawler_request_next(ScCrawlerRequest *request) {
  if(request && !g_tree_remove(request->self->requests, request->digest)) {
    g_critical(G_STRLOC ": invalid request %p (%s)", request, request->url);
    return;
  }
  g_print("[queue: %d] [requests: %d] finished: %s\n",
	  request->self->queue->len,
	  g_tree_nnodes(request->self->requests),
	  request->url);
  if(g_tree_nnodes(request->self->requests) <= 0) {
    if(request->self->queue->len > 0) {
      gchar *url = g_ptr_array_remove_index(request->self->queue,
					    request->self->queue->len - 1);
      request->self->nextcallback(url, request->data);
      g_free(url);
    }
    else {
      request->self->finishedcallback(request->data);
    }
  }
}

static void sc_crawler_request_free(ScCrawlerRequest *request) {
  g_debug(G_STRLOC ": finishing: %s", request->url);
  request->self->crawledcallback(request, request->data);
  if(request->url) g_free(request->url);
  if(request->uri) soup_uri_free(request->uri);
  if(request->contenttype) g_free(request->contenttype);
  if(request->parser) sc_xmlparser_free(request->parser);
  g_free(request);
}

static void sc_crawler_end_document_cb(gpointer user_data,
				       GError *error) {
  ScCrawlerRequest *request = user_data;
  sc_crawler_request_next(request);
}

static void sc_crawler_start_element_cb(const gchar *name,
					const gchar **attributes,
					gpointer user_data,
					GError *error) {
  ScCrawlerRequest *request = user_data;
  GString *prefix = g_string_new(NULL);
  gint iter = 0;

  while(attributes && attributes[iter]) {
    gchar *url = NULL;
    
    if(g_str_has_prefix(attributes[iter], "http://") ||
       g_str_has_prefix(attributes[iter], "https://")) {
      url = g_strdup(attributes[iter]);
    }
    else if(g_str_has_prefix(attributes[iter], "//")) {
      url = g_strdup_printf("%s:%s", soup_uri_get_scheme(request->uri), attributes[iter]);
    }
    else {
      if(!prefix->len) {
	const gchar *scheme = soup_uri_get_scheme(request->uri);
	const gchar *user = soup_uri_get_user(request->uri);
	const gchar *password = soup_uri_get_password(request->uri);
	const gchar *host = soup_uri_get_host(request->uri);
	guint port = soup_uri_get_port(request->uri);

	if(scheme) g_string_append_printf(prefix, "%s://", scheme);
	if(user) g_string_append_printf(prefix, "%s", user);
	if(password) g_string_append_printf(prefix, ":%s", password);
	if(user) g_string_append_c(prefix, '@');
	if(host) g_string_append(prefix, host);
	if(port) g_string_append_printf(prefix, ":%d", port);
      }

      if(attributes[iter][0] == '/') {
	url = g_strdup_printf("%s%s", prefix->str, attributes[iter]);
      }
      else if(iter > 0 &&
	      attributes[iter - 1] &&
	      (g_strcmp0("href", attributes[iter - 1]) == 0 ||
	       g_strcmp0("src", attributes[iter - 1]) == 0) &&
	      !g_str_has_prefix(attributes[iter], "mailto:") &&
	      !g_str_has_prefix(attributes[iter], "file://") &&
	      !g_str_has_prefix(attributes[iter], "javascript:") &&
	      !g_str_has_prefix(attributes[iter], "ftp:") &&
	      !g_str_has_prefix(attributes[iter], "data:") &&
	      attributes[iter][0] != '#') {
	url = g_strdup_printf("%s/%s", prefix->str, attributes[iter]);
      }
    }

    if(url) {
      request->self->newlinkfoundcallback(request, url, request->data);
      g_free(url);
    }
    
    iter++;
  }

  g_string_free(prefix, TRUE);
}

static void sc_crawler_get_response_cb(GInputStream *stream,
				       GAsyncResult *result,
				       gpointer user_data) {
  ScCrawlerRequest *request = user_data;
  GError *error = NULL;

  request->len = g_input_stream_read_finish(stream, result, &error);
  if(error) {
    g_critical(G_STRLOC ": %d: %s", error->code, error->message);
    g_error_free(error);
    g_input_stream_close(stream, NULL, NULL);
    sc_crawler_request_next(request);
    sc_crawler_request_free(request);
    return;
  }

  request->self->gotchunkcallback(request, request->data);
  if(request->contenttype &&
     g_str_has_prefix(request->contenttype, "text/html")) {
    if(!request->parser) {
      request->parser = sc_xmlparser_new(sc_crawler_start_element_cb,
					 sc_crawler_end_document_cb,
					 request);
    }
    if(request->len <= 0) {
      g_input_stream_close(stream, NULL, NULL);
    }
    sc_xmlparser_parse_chunk(request->parser, request->buffer, request->len);
  }

  if(request->len > 0) {
    request->chunkreceived = TRUE;
    memset(request->buffer, 0, SC_CRAWLER_BUFFERSIZE);
    g_input_stream_read_async(stream,
			      request->buffer,
			      SC_CRAWLER_BUFFERSIZE,
			      G_PRIORITY_DEFAULT,
			      NULL,
			      (GAsyncReadyCallback) sc_crawler_get_response_cb,
			      request);
  }
  else {
    if(!request->parser || !request->chunkreceived) {
      sc_crawler_request_next(request);
    }
    sc_crawler_request_free(request);
  }
}

static void sc_crawler_get_response(SoupMessage *message,
				    GInputStream *result,
				    gpointer user_data,
				    GError *error) {
  ScCrawlerRequest *request = user_data;
  if(error) {
    g_critical(G_STRLOC ": %d: %s", error->code, error->message);
    g_error_free(error);
    sc_crawler_request_next(request);
    sc_crawler_request_free(request);
    return;
  }

  if(!request->handleresponse) {
    g_input_stream_close(result, NULL, NULL);
    sc_crawler_request_next(request);
    sc_crawler_request_free(request);
    return;
  }

  g_input_stream_read_async(result,
			    request->buffer,
			    SC_CRAWLER_BUFFERSIZE,
			    G_PRIORITY_DEFAULT,
			    NULL,
			    (GAsyncReadyCallback) sc_crawler_get_response_cb,
			    request);
}

static void sc_crawler_got_headers_cb(SoupSession *session,
				      SoupMessage *message,
				      gpointer user_data) {
  ScCrawlerRequest *request = user_data;
  SoupMessageHeaders *responseheaders = NULL;
  const gchar *contenttype = NULL;
  
  g_object_get(message, "uri", &(request->uri), NULL);
  g_object_get(message, "status-code", &(request->statuscode), NULL);
  g_object_get(message, "response-headers", &responseheaders, NULL);
  contenttype = soup_message_headers_get_one(responseheaders,"Content-Type");
  if(contenttype) request->contenttype = g_strdup(contenttype);
  request->handleresponse = request->self->gotheaderscallback(request, request->data);
}

void sc_crawler_crawl(ScCrawler *self,
		      const gchar *url,
		      gpointer digest,
		      gpointer data) {
  ScCrawlerRequest *request = NULL;
  gint requests = g_tree_nnodes(self->requests);

  if(self->maxlinks &&
     self->nb_links++ >= self->maxlinks) {
    g_debug(G_STRLOC ": rejecting incoming links due to maxlinks limit %u nb_links: %u",
	    self->maxlinks,
	    self->nb_links);
    return;
  }
  
  if(requests > SC_CRAWLER_MAX_ASYNC_REQUESTS) {
    g_debug(G_STRLOC ": queued: %s", url);
    g_ptr_array_insert(self->queue, 0, g_strdup(url));
    return;
  }

  request = g_new0(ScCrawlerRequest, 1);
  request->url = g_strdup(url);
  request->digest = digest;
  request->data = data;
  request->self = self;
  g_tree_insert(self->requests, digest, digest);
  sc_httpclient_get_async(request->url,
			  sc_crawler_get_response,
			  sc_crawler_got_headers_cb,
			  request);
  g_print("[queue: %d] [requests: %d] started: %s\n",
	  self->queue->len, g_tree_nnodes(self->requests), url);
}

gint sc_crawler_compare_func(gconstpointer a,
			     gconstpointer b,
			     gpointer user_data) {
  gsize length = (gsize) user_data;
  return memcmp(a, b, length);
}

ScCrawler* sc_crawler_new(ScCrawlerFinishedCallback finishedcallback,
			  ScCrawlerGotHeadersCallback gotheaderscallback,
			  ScCrawlerGotChunkCallback gotchunkcallback,
			  ScCrawlerNewLinkFoundCallback newlinkfoundcallback,
			  ScCrawlerCrawledCallback crawledcallback,
			  ScCrawlerNextCallback nextcallback,
			  gsize maxlinks) {
  ScCrawler *self = g_new0(ScCrawler, 1);
  self->finishedcallback = finishedcallback;
  self->gotheaderscallback = gotheaderscallback;
  self->gotchunkcallback = gotchunkcallback;
  self->newlinkfoundcallback = newlinkfoundcallback;
  self->crawledcallback = crawledcallback;
  self->nextcallback = nextcallback;
  self->maxlinks = maxlinks;
  self->requests = g_tree_new_full(sc_crawler_compare_func,
				   (gpointer) g_checksum_type_get_length(G_CHECKSUM_MD5),
				   NULL,
				   NULL);
  self->queue = g_ptr_array_new();
  return self;
}

void sc_crawler_free(ScCrawler *self) {
  g_tree_destroy(self->requests);
  g_ptr_array_free(self->queue, FALSE);
  g_free(self);
}

#ifndef __SC_HTTPCLIENT__
#define __SC_HTTPCLIENT__

#include <libsoup/soup.h>

G_BEGIN_DECLS

#ifndef SC_HTTPCLIENT_MAX_CONNS_PER_HOST
#define SC_HTTPCLIENT_MAX_CONNS_PER_HOST 10
#endif

/*
 * ScHttpclientCallback - this function will be called after
 *                        receiving body
 *
 * message - SoupMessage object which contains connection information
 *           including response headers
 * stream - contains body of the response
 * user_data - user passed data object
 * error - error message from libsoup
 * 
 */
typedef void (*ScHttpclientCallback)(SoupMessage *message,
				     GInputStream *stream,
				     gpointer user_data,
				     GError *error);

/*
 * ScHttpclientGotHeadersCallback - this function will be called immediately
 *                                  after receiving headers, this can be
 *                                  used to check the headers before getting
 *                                  body
 *
 * session - SoupSession from libsoup, contains connection information.
 * message - SoupMessage which contains state of particular connection
 * user_data - user passed data object
 *
 */
typedef void (*ScHttpclientGotHeadersCallback)(SoupSession *session,
					       SoupMessage *message,
					       gpointer user_data);

typedef struct {
  gchar *url;
  SoupSession *session;
  SoupMessage *message;
  ScHttpclientCallback callback;
  ScHttpclientGotHeadersCallback gotheaderscallback;
  gpointer data;
} ScHttpclient;

/*
 * sc_httpclient_get_async - function to start receiving data 
 *                           for a particular url
 * 
 * url - url to connect and fetch data
 * callback - function to call once body received
 * gotheaderscallback - function to call once header received
 * user_data - object to pass to callback functions
 *
 */
gboolean sc_httpclient_get_async(const gchar *url,
				 ScHttpclientCallback callback,
				 ScHttpclientGotHeadersCallback gotheaderscallback,
				 gpointer user_data);

G_END_DECLS

#endif /* __SC_HTTPCLIENT__ */

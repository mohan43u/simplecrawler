#ifndef __SC_XMLPARSER__
#define __SC_XMLPARSER__

#include <libxml/HTMLparser.h>

G_BEGIN_DECLS

/*
 * ScXmlParserStartElementCallback - this function will be called
 *                                   everytime a start tag (like <a href="something">)
 *                                   parsed by xmlparser
 *
 * name - name of the tag (like html, head etc.,)
 * attributes - null terminated string array of attributes where
 *              first element is attribute name and next element is
 *              attribute value.
 * user_data - user passed object
 * error - error from xmlparser
 *
 */
typedef void (*ScXmlParserStartElementCallback)(const gchar *name,
						const gchar **attributes,
						gpointer user_data,
						GError *error);

/*
 * ScXmlParserEndDocumentCallback - this function will be called once
 *                                  whole html document completed (reaching </html>)
 *
 * user_data - user provided object
 * error - error if any from xmlparser
 */
typedef void (*ScXmlParserEndDocumentCallback)(gpointer user_data,
					       GError *error);

typedef struct {
  htmlSAXHandler handler;
  htmlParserCtxtPtr context;
  ScXmlParserStartElementCallback startelementcallback;
  ScXmlParserEndDocumentCallback enddocumentcallback;
  gpointer user_data;
} ScXmlParser;

/*
 * sc_xmlparser_new - function to create new parser
 * 
 * ScXmlParserStartElementCallback - function to call everytime a start tag parsed
 * ScXmlParserEndDocumentCallback - function to call at the end of parsing
 * user_data - user provided object
 *
 */
ScXmlParser* sc_xmlparser_new(ScXmlParserStartElementCallback startelementcallback,
			      ScXmlParserEndDocumentCallback enddocumentcallback,
			      gpointer user_data);

/*
 * sc_xmlparser_parse_chunk - function used to feed libxml2 SAX parser
 * 
 * chunk - html arbitary data to parser
 * size - size of html data
 *
 */
gboolean sc_xmlparser_parse_chunk(ScXmlParser *self,
				  const gchar *chunk,
				  gsize size);

/*
 * sc_xmlparser_free - to cleanup parser
 *
 * self - parser object
 *
 */
void sc_xmlparser_free(ScXmlParser *self);

G_END_DECLS

#endif /* __SC_XMLPARSER__ */

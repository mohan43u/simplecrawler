#ifndef __SC_CRAWLER__
#define __SC_CRAWLER__

#include "httpclient.h"
#include "xmlparser.h"
#include "localstoragemanager.h"

G_BEGIN_DECLS

#ifndef SC_CRAWLER_BUFFERSIZE
#define SC_CRAWLER_BUFFERSIZE 4098
#endif

#ifndef SC_CRAWLER_MAX_ASYNC_REQUESTS
#define SC_CRAWLER_MAX_ASYNC_REQUESTS 1000
#endif

#ifndef SC_CRAWLER_MAX_LINKS
#define SC_CRAWLER_MAX_LINKS 10
#endif

typedef struct _ScCrawlerRequest ScCrawlerRequest;

/*
 * ScCrawlerFinishedCallback - function to call when crawler completes all links
 * user_data - user provided object
 *
 */
typedef void (*ScCrawlerFinishedCallback)(gpointer user_data);

/*
 * ScCrawlerGotHeadersCallback - function to call once header received
 *
 * request - crawler request object, contains url, 
 *           statuscode, response headers, etc.,
 * user_data - user provided object
 * 
 */
typedef gboolean (*ScCrawlerGotHeadersCallback)(ScCrawlerRequest *request,
					       gpointer user_data);

/*
 * ScCrawlerGotChunkCallback - function to call once chunk of body received
 *                             request object passed in this call contains
 *                             chunk in "buffer" property which is of size
 *                             SC_CRAWLER_BUFFERSIZE.
 *
 * request - crawler request object, contains url, 
 *           statuscode, response headers, etc.,
 * user_data - user provided object
 * 
 */
typedef void (*ScCrawlerGotChunkCallback)(ScCrawlerRequest *request,
					  gpointer user_data);

/*
 * ScCrawlerNewLinkFoundCallback - function to call whenever a new link
 *                                 parsed by xmlparser
 *
 * request - crawler request object, contains url, 
 *           statuscode, response headers, etc.,
 * url - new url taken from parsing the html page
 * user_data - user provided object
 * 
 */
typedef void (*ScCrawlerNewLinkFoundCallback)(ScCrawlerRequest *request,
					      const gchar *url,
					      gpointer user_data);

/*
 * ScCrawlerCrawledCallback - function to call everytime a url
 *                            fetching completes.
 *
 * request - crawler request object, contains url, 
 *           statuscode, response headers, etc.,
 * user_data - user provided object
 * 
 */
typedef void (*ScCrawlerCrawledCallback)(ScCrawlerRequest *request,
					 gpointer user_data);

/*
 * ScCrawlerNextCallback - function to call everytime a new
 *                         user taken out from pending queue.
 *
 * url - url that was stored previously in a queue for
 *       later fetching.
 * user_data - user provided object
 * 
 */
typedef void (*ScCrawlerNextCallback)(const gchar *url,
				      gpointer user_data);

typedef struct {
  ScHttpclient *httpclient;
  ScCrawlerFinishedCallback finishedcallback;
  ScCrawlerGotHeadersCallback gotheaderscallback;
  ScCrawlerGotChunkCallback gotchunkcallback;
  ScCrawlerNewLinkFoundCallback newlinkfoundcallback;
  ScCrawlerCrawledCallback crawledcallback;
  ScCrawlerNextCallback nextcallback;
  gsize maxlinks;
  gsize nb_links;
  GTree *requests;
  GPtrArray *queue;
} ScCrawler;

struct _ScCrawlerRequest {
  gchar *url;
  gpointer digest;
  guint statuscode;
  gpointer data;
  ScCrawler *self;
  ScXmlParser *parser;
  SoupURI *uri;
  gchar *contenttype;
  gboolean handleresponse;
  gboolean chunkreceived;
  gchar buffer[SC_CRAWLER_BUFFERSIZE];
  gint len;
};

ScCrawler* sc_crawler_new(ScCrawlerFinishedCallback finishedcallback,
			  ScCrawlerGotHeadersCallback gotheaderscallback,
			  ScCrawlerGotChunkCallback gotchunkcallback,
			  ScCrawlerNewLinkFoundCallback newlinkfoundcallback,
			  ScCrawlerCrawledCallback crawledcallback,
			  ScCrawlerNextCallback nextcallback,
			  gsize maxlinks);
/*
 * sc_crawler_crawl - start crawling the url
 *
 * self - ScCrawler Object
 * url - url to crawl
 * digest - md5 digest, this function takes ownership of the digest data
 *          so caller should not free() otherwise you will see segfault.
 * user_data - user provided object
 */
void sc_crawler_crawl(ScCrawler *self,
		      const gchar *url,
		      const gpointer digest,
		      gpointer user_data);
void sc_crawler_free(ScCrawler *self);
/*
 * sc_crawler_compare_func - used by GTree(balenced binary-tree implementation)
 *
 * a - pointer to MD5 hash which was generated from a url string
 * b - another pointer to MD5 hash which was generated from a url string
 * user_data - length of MD5 has will be passed here
 */
gint sc_crawler_compare_func(gconstpointer a,
			     gconstpointer b,
			     gpointer user_data);

G_END_DECLS

#endif /* __SC_CRAWLER__ */

#include <string.h>
#include <glib-unix.h>
#include "localstoragemanager.h"

ScLocalStorageManager* sc_local_storage_manager_new(const gchar *url) {
  ScLocalStorageManager *manager = g_new0(ScLocalStorageManager, 1);
  SoupURI *uri = soup_uri_new(url);
  GString *filename = g_string_new(NULL);
  const gchar *host = soup_uri_get_host(uri);
  const gchar *path = soup_uri_get_path(uri);
  const gchar *query = soup_uri_get_query(uri);
  const gchar *fragment = soup_uri_get_fragment(uri);
  GFile *parent = NULL;
  GError *error = NULL;

  g_string_append_printf(filename, "%s/%s", host, path);
			 
  if(url[strlen(url) - 1] == '/' || g_strcmp0("/", path) == 0)
    g_string_append(filename, "/index.html");
  
  if(query) g_string_append_printf(filename, "/%s\n", query);
  if(fragment) g_string_append_printf(filename, "#%s\n", fragment);
  manager->file = g_file_new_for_commandline_arg(filename->str);
  g_string_free(filename, TRUE);

  if(g_file_query_exists(manager->file, NULL)) {
    g_file_delete(manager->file, NULL, &error);
    if(error && !g_error_matches(error, G_IO_ERROR,G_IO_ERROR_EXISTS)) {
      g_critical(G_STRLOC ": %d : %s", error->code, error->message);
      g_error_free(error);
      g_object_unref(parent);
      g_object_unref(manager->file);
      g_free(manager);
      return NULL;
    }
  }

  parent = g_file_get_parent(manager->file);
  g_file_make_directory_with_parents(parent, NULL, &error);
  if(error && !g_error_matches(error, G_IO_ERROR,G_IO_ERROR_EXISTS)) {
    g_critical(G_STRLOC ": %d : %s", error->code, error->message);
    g_error_free(error);
    g_object_unref(parent);
    g_object_unref(manager->file);
    g_free(manager);
    return NULL;
  }

  error = NULL;
  manager->ostream = g_file_create(manager->file, G_FILE_CREATE_NONE, NULL, &error);
  if(error) {
    g_critical(G_STRLOC ": %d : %s", error->code, error->message);
    g_error_free(error);
    g_object_unref(parent);
    g_object_unref(manager->file);
    g_free(manager);
    return NULL;
  }
  return manager;
}

void sc_local_storage_manager_handle_chunk(ScLocalStorageManager *manager,
					   const gchar *buffer,
					   gint len) {
  GError *error = NULL;
  g_output_stream_write_all(G_OUTPUT_STREAM(manager->ostream),
				  buffer,
				  len,
				  NULL,
				  NULL,
				  &error);
  if(error) {
    g_critical(G_STRLOC ": %d : %s", error->code, error->message);
    g_error_free(error);
  }
}

void sc_local_storage_manager_free(ScLocalStorageManager *manager) {
  GError *error = NULL;
  g_output_stream_close(G_OUTPUT_STREAM(manager->ostream), NULL, &error);
  if(error) {
    g_critical(G_STRLOC ": %d: %s", error->code, error->message);
    g_error_free(error);
  }
  g_object_unref(manager->file);
  g_free(manager);
}

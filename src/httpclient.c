#include <string.h>
#include <glib-unix.h>
#include <glib-object.h>
#include "httpclient.h"

static void sc_httpclient_finished_cb(SoupMessage *msg,
				      gpointer user_data) {
  ScHttpclient *request = user_data;
  g_debug(G_STRLOC ":cleaning: %s", request->url);
  g_object_unref(request->message);
  g_object_unref(request->session);
  g_free(request->url);
  g_free(request);
}

static void sc_httpclient_got_headers_cb(SoupSession *session,
					 gpointer user_data) {
 ScHttpclient *request = user_data;
 request->gotheaderscallback(request->session,
			     request->message,
			     request->data);
}

static void sc_httpclient_get_async_cb(SoupSession *session,
				       GAsyncResult *result,
				       gpointer user_data) {
  ScHttpclient *request = user_data;
  GInputStream *inputstream = NULL;
  GError *error = NULL;
  inputstream = soup_session_send_finish(session, result, &error);
  request->callback(request->message, inputstream, request->data, error);
}

gboolean sc_httpclient_get_async(const gchar *url,
				 ScHttpclientCallback callback,
				 ScHttpclientGotHeadersCallback gotheaderscallback,
				 gpointer user_data) {
  ScHttpclient *request = g_new0(ScHttpclient, 1);
  SoupLoggerLogLevel loglevel = SOUP_LOGGER_LOG_NONE;
  SoupURI *proxyuri = NULL;
  const gchar *http_proxy = NULL;
  const gchar *debug = NULL;
  
  request->session = soup_session_new();
  debug = g_getenv("SOUP_DEBUG");

  if(g_strcmp0("MINIMAL", debug) == 0) loglevel = SOUP_LOGGER_LOG_MINIMAL;
  if(g_strcmp0("HEADERS", debug) == 0) loglevel = SOUP_LOGGER_LOG_HEADERS;
  if(g_strcmp0("BODY", debug) == 0) loglevel = SOUP_LOGGER_LOG_BODY;

  if(loglevel != SOUP_LOGGER_LOG_NONE) {
    SoupLogger *logger = soup_logger_new(loglevel, -1);
    soup_session_add_feature(request->session, SOUP_SESSION_FEATURE(logger));
  }

  http_proxy = g_getenv("http_proxy");
  if(http_proxy) {
    proxyuri = soup_uri_new(http_proxy);
    g_object_set(request->session, "proxy-uri", proxyuri, NULL);
  }

  request->url = g_strdup(url);
  if(!(request->message = soup_message_new("GET", request->url))) {
    g_critical(G_STRLOC ": %s malformed", request->url);
    return FALSE;
  }
  request->callback = callback;
  request->gotheaderscallback = gotheaderscallback;
  request->data = user_data;
  g_signal_connect(request->message, "finished", (GCallback) sc_httpclient_finished_cb, request);
  g_signal_connect(request->message, "got-headers", (GCallback) sc_httpclient_got_headers_cb, request);
  soup_message_set_flags(request->message, SOUP_MESSAGE_IGNORE_CONNECTION_LIMITS);

  soup_session_send_async(request->session, request->message, NULL,
			  (GAsyncReadyCallback) sc_httpclient_get_async_cb, request);
  return TRUE;
}

#ifndef __SC_LOCAL_STORAGE_MANAGER__
#define __SC_LOCAL_STORAGE_MANAGER__

#include <libsoup/soup.h>

G_BEGIN_DECLS

typedef struct {
  GFile *file;
  GFileOutputStream *ostream;
  gpointer buffer;
} ScLocalStorageManager;

/*
 * sc_local_storage_manager_new - function to create manager object
 *                                this object takes care of creating
 *                                filename from url and storing data
 */
ScLocalStorageManager* sc_local_storage_manager_new(const gchar *uri);
/*
 * sc_local_storage_manager_handle_chunk - function to update the manager whenever
 *                                         chunk of data received from crawler
 * buffer - buffer contains data
 * len - length of buffer
 *
 */
void sc_local_storage_manager_handle_chunk(ScLocalStorageManager *manager,
					   const gchar *buffer,
					   gint len);
/*
 * sc_local_storage_manager_free - function to cleanup storage manager.
 */
void sc_local_storage_manager_free(ScLocalStorageManager *manager);

G_END_DECLS

#endif /* __SC_LOCAL_STORAGE_MANAGER__ */

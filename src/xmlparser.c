#include <string.h>
#include <glib-unix.h>
#include "xmlparser.h"
#include "httpclient.h"

static void sc_xmlparser_start_element_cb(void *ctx,
					  const xmlChar *name,
					  const xmlChar **attributes) {
  ScXmlParser *self = ctx;
  self->startelementcallback(name, (const gchar **) attributes, self->user_data, NULL);
}

static void sc_xmlparser_end_document_cb(void *ctx) {
  ScXmlParser *self = ctx;
  self->enddocumentcallback(self->user_data, NULL);
}

ScXmlParser* sc_xmlparser_new(ScXmlParserStartElementCallback startelementcallback,
			      ScXmlParserEndDocumentCallback enddocumentcallback,
			      gpointer user_data) {
  ScXmlParser *parser = g_new0(ScXmlParser, 1);
  parser->handler.initialized = XML_SAX2_MAGIC;
  parser->handler.startElement = sc_xmlparser_start_element_cb;
  parser->handler.endDocument = sc_xmlparser_end_document_cb;
  parser->startelementcallback = startelementcallback;
  parser->enddocumentcallback = enddocumentcallback;
  parser->user_data = user_data;
  return parser;
}

static gboolean sc_xmlparser_handle_error(xmlError error) {
  g_debug(G_STRLOC ": %d: %d: %d: %s", error.code, error.line, error.int2, error.message);
  return error.level != XML_ERR_FATAL;
}

gboolean sc_xmlparser_parse_chunk(ScXmlParser *self,
				  const gchar *chunk,
				  gsize size) {
  g_debug("%s", chunk);
  if(!self->context) {
    if(!(self->context = htmlCreatePushParserCtxt(&(self->handler),
						  self, chunk, size, NULL,
						  XML_CHAR_ENCODING_UTF8))) {
      return sc_xmlparser_handle_error(self->context->lastError);
    }
    htmlCtxtUseOptions(self->context, HTML_PARSE_IGNORE_ENC);
    return TRUE;
  }
  if((htmlParseChunk(self->context, chunk, size, (size > 0 ? 0 : 1)))) {
    return sc_xmlparser_handle_error(self->context->lastError);
  }
  return TRUE;
}

void sc_xmlparser_free(ScXmlParser *self) {
  htmlFreeParserCtxt(self->context);
  xmlCleanupParser();
  g_free(self);
}

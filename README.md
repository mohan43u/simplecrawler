simplecrawler
=============

yet another web crawler written in c. It is a library (libsimplecrawler) with
following features,

1. collects all links from a html page and cycle through the links
2. It can be enabled for headers only fetching if a link points to non html content
3. It can be enabled for full content retrival and capable of saving the content to local disk
4. asynchronous fetching.

dependencies
============

1. libsoup-2.4
2. libxml-2.0
3. gio-2.0
4. glib-2.0
5. gobject-2.0

compilation:
============

1. git clone https://github.com/mohan43u/simplecrawler.git
2. cd simplecrawler
3. ./configure
4. make

example commandlines:
====================

    $ ./simplecrawler 0 0 python.org # process all links from python.org
    $ ./simplecrawler 10000 0 python.org # process only first 10000 links from python.org
    $ ./simplecrawler 100 1 python.org # store contents of first 100 links from python.org to local directory www.python.org
